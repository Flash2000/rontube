def add_file(filet, file_to_add):
    with open(filet, 'r+') as f:
        lines = f.readlines()
    with open(filet, 'w+') as f:
        for line in lines:
            if line.split() == ["</select>"]:
                f.write("    <option value=" + file_to_add.replace(" ", "_") + ">" + file_to_add[:-4] + "</option>\n")
                print 1
            f.write(line)

if __name__ == "__main__":
    add_file("test.html", "hello.mp3")
