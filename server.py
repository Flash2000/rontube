import tornado.ioloop
import tornado.web
import os, uuid
import add_to_database


class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user")


# class LoginHandler(BaseHandler):
#     def get(self):
#         self.render("index.html")

global name
name = ""
class MainHandler(BaseHandler):
    def get(self):
        self.redirect("/start")


class Userform(tornado.web.RequestHandler):
    def get(self):
        #self.write("Hello, " + Name)
        self.render("design.html")

global song_name
song_name = ""


# This page allows you to watch a video
class Watch(tornado.web.RequestHandler):
    def post(self):
		song_name = self.get_body_argument("songlist")
		print song_name
		self.redirect("/static/"+song_name)

__UPLOADS__ = "database/"
class Upload(tornado.web.RequestHandler):
    def post(self):
        fileinfo = self.request.files['filearg'][0]
        print "fileinfo is", fileinfo
        fname = fileinfo['filename']
        extn = os.path.splitext(fname)[1]
        fh = open(__UPLOADS__ + fname.replace(" ","_"), 'w')
        fh.write(fileinfo['body'])
        add_to_database.add_file("design.html", fname)
        self.render("goBackToHome.html")

class SuccesfullUpload(tornado.web.RequestHandler):
    def get(self):
        self.render("goBackToHome.html")

settings = {
	"static_path": os.path.join(os.path.dirname(__file__), "database/"),
    "script_path": "script.js",
	"cookie_secret": "61oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
	# "login_url": "/login",
	"xsrf_cookies": False,
}


application = tornado.web.Application([

        (r"/start", Userform),
        (r"/watch", Watch),
        (r"/upload", Upload),
        (r"/success", SuccesfullUpload),
        (r"/", MainHandler),
        # (r"/login", LoginHandler),
		(r"/"+song_name, tornado.web.StaticFileHandler, dict(path=settings["static_path"])),
        (r"/script\.js", tornado.web.StaticFileHandler, dict(path=settings["script_path"]))
        ], debug=True, **settings)

if __name__ == "__main__":
    application.listen(80)
    tornado.ioloop.IOLoop.current().start()
